# Development
At this point you will have a directory for your gradle project. 
The current structure of your folder should look a bit like 

````
├── 1_setup.md
├── 2_create_project.md
├── 3_development.md
├── README.md
├── intro-scala.iml
└── myproject
    ├── app
    │   ├── build.gradle
    │   └── src
    │       ├── main
    │       │   ├── resources
    │       │   └── scala
    │       │       └── myproject
    │       │           └── App.scala
    │       └── test
    │           ├── resources
    │           └── scala
    │               └── myproject
    │                   └── AppSuite.scala
    ├── gradle
    │   └── wrapper
    │       ├── gradle-wrapper.jar
    │       └── gradle-wrapper.properties
    ├── gradlew
    ├── gradlew.bat
    └── settings.gradle


````

You will now be working inside the myproject directory. 

# Add a new function to Scala Code
Go to App.scala At the moment this will look like 

````
object App {
  def main(args: Array[String]): Unit = {
    println(greeting())
  }

  def greeting(): String = "Hello, world!"
}

````

We are going to add a new function that adds two numbers and returns another
and call it from our main function. 

Ths function will look like this

````
  def add_and_double(x: Int, y: Int): Int = {
    // adds together two numbers and then returns double the result    
    val add_numbers = x + y
    add_numbers * 2
  }
````

And once we've called it App.scala will now look like this

```
object App {
  def main(args: Array[String]): Unit = {
    println(greeting())
    val add_double_result = add_and_double(5, 6) //result is 22
    println(add_double_result)

  }

  def greeting(): String = "Hello, world!"

  def add_and_double(x: Int, y: Int): Int = {
    // adds together two numbers and then returns double the result
    val add_numbers = x + y
    add_numbers * 2
  }
}
```

We shall also add a test to our project testing suite under AppSuite.scala

This will look like 

```
package myproject

import org.scalatest.funsuite.AnyFunSuite
import org.junit.runner.RunWith
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class AppSuite extends AnyFunSuite {

  test("App has a greeting") {
    assert(App.greeting() != null)
  }
  
  
  test("NEW TEST FOR MY FUNCTION") {
    assert(App.add_and_double(5, 6) == 22)
  }
}
```

To run the test for this run 

    gradle run

this should look like 

```
Discovery starting.
Discovery completed in 222 milliseconds.
Run starting. Expected test count is: 2
AppSuite:
- App has a greeting (19 milliseconds)
- add_double_function (0 milliseconds)
Run completed in 323 milliseconds.
Total number of tests run: 2
Suites: completed 2, aborted 0
Tests: succeeded 2, failed 0, canceled 0, ignored 0, pending 0
All tests passed.

```

# Complete
We have now modified a scala project, added a function to it and created a test for the function so we know that 
if all tests suceed then our application is always going to do what is expected. Next
we shall move on to packaging the application into a docker container so it can be deployed. git remote add origin https://github.com/TRReeve/intro-gradle.git