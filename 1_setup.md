# HOW TO SET UP A SCALA ENVIRONMENT
# 1. Install Java (We will install Java 8 as that's the recommended version but others work too)

    brew cask install adoptopenjdk8
    
Check Install worked 

    java -version
    
For windows users I found it easiest to use choco (https://chocolatey.org/install)then 

    choco install jdk8
    
OR 
    
    choco install openjdk8 

mainly because oracle are known bastards so openjdk8 may be the safer bet.

# 2. Install scala

Because gradle will handle the scala installation for you you don't actually need to install it yourself
however it can be useful to have installed locally anyway. 

https://www.scala-lang.org/download/
I'd recommend using SDKMAN as it lets you easily deal with multiple different versions which is more necessary as things like
Spark are only compatable up to 2.12 as of 2020-11-24. 

# 3. Install Gradle
https://gradle.org/install/

If you used SDKMAN you can also use it to install Gradle here as well. For now these instructions work with any version after 
Gradle 6.7

For windows

    choco install gradle


# Done
At this point you should have java, scala and gradle all installed on your computer. You now have everything in place to start
developing in Java or Scala (and a lot of other languages too)

In section 2 We'll initialise a project. 

