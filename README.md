## Intro-Scala
This repo aims to demonstrate how to set up a scala project for an application/spark job etc. We will not get deep into
the weeds of Scala or functional programming but will aim to create and run a solid development environment in which we can 
create a Hello World level programme with some tests and deploy it to a docker container from where your options are very wide.

# Prerequisites
The aim here and one of the advantages of Gradle and the JVM (the magical box that Java and Scala and Kotlin and Clojure et al all run in) 
is that we aren't reliant on any particular IDE or OS to run this. However I'm writing this on a Mac and using IntelliJ's IDE. Any unix
terminal should be sufficient although gradle can also be run on windows. I will write most of the commands in unix as windows has .exe
packages for the key installation components like Java anyway that can be installed easily.


# 1. Setup
You will install Java, then the scala language, then 
Gradle a cross-platform package builder for deploying Programmes (it is also a very popular tool for Java)

# 2. Project initialisation
Once these are complete we will then initialise a gradle project using scala and run it using terminal commands

# 3. Development
Once we have our project running we will then add our own function and write a test for it.

# 4. Packaging and deployment
Once we have our basic application working and testing. Then we will use a plugin called shadowjar to create a self-contained 
Jar File and put that inside a docker container to run an application in docker. 
