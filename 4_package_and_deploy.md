# Packaging and Deployment

The final, often overlooked stage of building an application is packaging and deploying it. 
To achieve this here we will construct a self-contained Java package known as a fat-jar.
We will then further package this inside a docker container which can be easily configured and 
run in a reproducible manner both locally and in a container orchestrator like ECS or kubernetes. 

# 1. Configure Shadow Jar Plugin
Add the gradle shadowjar plugin to your build.gradle file
https://plugins.gradle.org/plugin/com.github.johnrengelman.shadow
Your plugins section of build.gradle will look like so 

User guide to shadowjar: https://imperceptiblethoughts.com/shadow/introduction/#benefits-of-shadow

```
plugins {
    // Apply the scala Plugin to add support for Scala.
    id 'scala'

    // Apply the application plugin to add support for building a CLI application in Java.
    id 'application'
    id "com.github.johnrengelman.shadow" version "6.1.0"

}
```

# 2. Build Fat Jar
Configure the gradle shadowjar to output your desired jar file

```
shadowJar {
    zip64 = true
    archiveClassifier.set("")
    archiveVersion = ''
    mainClassName = 'myproject.App'
    mergeServiceFiles()
}
``` 

All things being equal this will output a jar file called app.jar that you should be able to run with

     java -jar .\app\build\libs\app.jar
     
This will run your program

# 3. (Optional) Build docker container
At this point you could stop working in gradle and have your docker container
defined the usual way and just use gradle to build your jar file then copy it to a docker container, 
in this step we will use gradle to link together our jar creation and building a container
to run our application in. 

The first thing to do will be to define a Dockerfile that will copy any .jar file we provide
in our docker build context to /app.jar 

myproject/app/Dockerfile
```
FROM adoptopenjdk/openjdk8

COPY *.jar /app.jar

ENTRYPOINT  ["java", "-jar",  "/app.jar"]
```

Then we shall add our docker plugin to our gradle definition
we will use the palantir docker plugin
https://github.com/palantir/gradle-docker

build.gradle
```
plugins {
    // Apply the scala Plugin to add support for Scala.
    id 'scala'

    // Apply the application plugin to add support for building a CLI application in Java.
    id 'application'
    id "com.github.johnrengelman.shadow" version "6.1.0"
    id 'com.palantir.docker' version '0.25.0'   
}
```

Notice that we also still have our shadowjar creation plugin as well.

Then we define our docker container task. 

myproject/app/build.gradle
```
docker {
    name "gradle-intro"
    files "build/libs/"
}
```

Here we define the name of our container, and we can define what files
we want available to our docker build context (e.g here build/libs is where our app.jar file goes to)

A next layer beyond this is if we link the docker task to our shadowjar task
we can then automatically build our jar file and our container all in one go by making 
sure that docker task only runs after running the shadowjar task

```
docker {
    name "gradle-intro"
    files tasks.shadowJar.outputs
    dependsOn(shadowJar)
}
```

Now run 

    gradle docker
    
    docker run gradle-intro
    
and you should get 

    Hello, world!
    

# 4. (Optional) Sequencing deployment

If you don't want to even type in gradle clean and gradle docker etc. You can also add a default argument
which will then allow you to just write 

    gradle
    
and have it perform several tasks for you

e.g here we define that we want to clean and then test and then build a container

build.gradle
```
defaultTasks 'clean', 'test', 'docker'
```

Combine that with our setup of testing earlier and we have a very effective deployment pipeline
for any application where we could also set up an automated push to a defined docker repository

# Complete 
You have now initialised a gradle project, made some changes to scala,
written a test, packaged that application to a self-contained jar file. 
And written that jar file to a docker container. 

Your final build.gradle looks like this
```
plugins {
    // Apply the scala Plugin to add support for Scala.
    id 'scala'

    // Apply the application plugin to add support for building a CLI application in Java.
    id 'application'
    id "com.github.maiflai.scalatest" version "0.25"
    id "com.github.johnrengelman.shadow" version "6.1.0"
    id 'com.palantir.docker' version '0.25.0'

}

repositories {
    // Use JCenter for resolving dependencies.
    jcenter()
}

dependencies {
    // Use Scala 2.13 in our library project
    implementation 'org.scala-lang:scala-library:2.13.3'

    // This dependency is used by the application.
    implementation 'com.google.guava:guava:29.0-jre'

    // Use Scalatest for testing our library
    testImplementation 'junit:junit:4.12'
    testImplementation 'org.scalatest:scalatest_2.13:3.2.0'
    testImplementation 'org.scalatestplus:junit-4-12_2.13:3.2.0.0'
    // https://mvnrepository.com/artifact/com.vladsch.flexmark/flexmark-all
    testImplementation 'com.vladsch.flexmark:flexmark-all:0.35.10'

    // Need scala-xml at test runtime
    testRuntimeOnly 'org.scala-lang.modules:scala-xml_2.13:1.2.0'
}

application {
    // Define the main class for the application.
    mainClass = 'myproject.App'
}

shadowJar {
    zip64 = true
    archiveClassifier.set("")
    archiveVersion = ''
    mainClassName = 'myproject.App'
    mergeServiceFiles()
}

docker {
    name "gradle-intro"
    files tasks.shadowJar.outputs
    dependsOn(shadowJar)
}

defaultTasks 'clean', 'test', 'docker'

```

and if you cd into the app directory

Run all tests and Build your docker image

    gradle
    
run docker image 

    docker run gradle-intro
    # Hello, world!
    
    

# Next steps
Yes you can manage several gradle projects within one repo using settings.gradle
https://docs.gradle.org/current/userguide/multi_project_builds.html

Yes you can add push and run steps to the docker plugin with other components
to the palantir plugin



