# Project Creation
Now that our main tools are installed. We will create initialise a gradle project and run it. Worth noting 
but most good IDEs will also have their own plugins for working with gradle. For now we will do this using terminal
commands mainly as these commands work on the most different operating systems. 


# 1. Create a project directory
This is the director where all of your work will reside. normally we would create the project as the "top level" of your 
repository but here the top level is where our instructions are so we will create a sub-directory for our code. You will
have to be in that directory to run gradle commands using the command line. 

    # Create a directory for your work
    mkdir myproject
    
    # cd into this directory
    cd myproject
    
    # our first gradle command to initialise a gradle template scala project
    # Follow the instructions using the defaults for now. This will do a lot of gruntwork
    # for you in terms of setting up .gitignore and all the stuff you need to start working
    # By default this will initialise with the latest version of scala but you can also initialise a different
    version of a language e.g 
    gradle init --type scala-application
    OR 
    gradle init
    and follow the instructions for an application
    
    # As of gradle 6.7.1 Your project should have been initialised with a "hello world" program already 
    # built for you. You should be able to run this with 
    gradle run
    This will look like 
    $ gradle run (or from the directory above gradle app:run)
    
    > Task :app:run
    Hello, world!
    
    BUILD SUCCESSFUL in 1s
    
# Set up Testing
Our first bit of work that requires some actual work. We will integrate scalatest to our 
project to provide very sophisticated testing functionality and built in readouts and logging of testing to our
project. 

    # Go to maven page and add the Gradle url to dependencies section in build.gradle
    (e.g // https://mvnrepository.com/artifact/com.vladsch.flexmark/flexmark
         compile group: 'com.vladsch.flexmark', name: 'flexmark', version: '0.62.2')
    Vladsch Flexmark 
    testImplementation 'com.vladsch.flexmark:flexmark-all:0.35.10'
    Scalatest
    https://mvnrepository.com/artifact/org.scalatest/scalatest
    
    # Follow instructions to add to Plugins section in build.gradle
    https://plugins.gradle.org/plugin/com.github.maiflai.scalatest
    
Once you're done your build.gradle dependencies should look like 

````
dependencies {
    // Use Scala 2.13 in our library project
    implementation 'org.scala-lang:scala-library:2.13.3'

    // This dependency is used by the application.
    implementation 'com.google.guava:guava:29.0-jre'
    
    // Use Scalatest for testing our library
    testImplementation 'junit:junit:4.12'
    testImplementation 'org.scalatest:scalatest_2.13:3.2.0'
    testImplementation 'org.scalatestplus:junit-4-12_2.13:3.2.0.0'
    // https://mvnrepository.com/artifact/com.vladsch.flexmark/flexmark-all
    testImplementation 'com.vladsch.flexmark:flexmark-all:0.35.10'
    
    // Need scala-xml at test runtime
    testRuntimeOnly 'org.scala-lang.modules:scala-xml_2.13:1.2.0'
}
````

and plugins will look like 

````
plugins {
    // Apply the scala Plugin to add support for Scala.
    id 'scala'

    // Apply the application plugin to add support for building a CLI application in Java.
    id 'application'
    id "com.github.maiflai.scalatest" version "0.25"

}
````

Confirm this works by running 

    gradle clean test
    
    # This should look something like 
    gradle clean test
    
    > Task :app:test
    Discovery starting.
    Discovery completed in 291 milliseconds.
    Run starting. Expected test count is: 2
    AppSuite:
    - App has a greeting (26 milliseconds)
    - 1 + 1 = 2 (2 milliseconds)
    Run completed in 484 milliseconds.
    Total number of tests run: 2
    Suites: completed 2, aborted 0
    Tests: succeeded 2, failed 0, canceled 0, ignored 0, pending 0
    All tests passed.

    
    
    
# Complete
You now have a running gradle project. You will also have noted that gradle allows you to run lots of different things just by running 
gradle $DOTHING. This You can customise these instructions in build.gradle as well as manage dependencies.
Notice also that gradle is creating "build" directories where files and libraries are created. Manipulation of these 
are how you can get very sophisticated builds and compilation of code without huge amounts of gruntwork and a gurantee that you 
can run the exact same build on some other computer or a Jenkins server etc and be confident it will work the same.
